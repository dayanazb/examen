$(document).ready(function(){

});

var Product = {
	delete: function(product_id){
    return $.getJSON('/product/'+product_id+'/delete');
  },
}

$('.a_delete').on("click",function(){
	
    var confirmLeave = confirm('¿Seguro que desea eliminar producto?');
	if (confirmLeave==true)
	{
  		var product_id = $(this).attr("id");
  		Product.delete(product_id).done(function(json){
  			if (json.code == 200) {
  				location.reload();
  			}
  		});
	}
});
