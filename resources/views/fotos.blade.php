@extends('layouts.app')

@section('fotos')
    <div class="container">
        {!! Form::open(['url' => '/vehiculo', 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
              			<div class="card-header">

                  					<div class="row">
							          	<div class="form-group col-md-8">
					              			<div class="col-md-6">
					                			<input type="file"  name="fotos">
					              			</div>
					           			</div>
							        

							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Subir</button>
							          </div>
							        </div>
                  		</div>
                  		<a href="/vehiculo" class="btn btn-info">Anterior</a>
                  		<a href="/cliente" class="btn btn-info">Salvar</a>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection