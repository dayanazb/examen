@extends('layouts.app')

@section('clientes')
  <div class="container">
      <div class="row justify-content-center">

          <div class="col-sm-8">
              <div class="card">

                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                      {!! Form::open(['url' => '/cliente', 'method' => 'POST']) !!}
                      <div class="form-group">
                        {!! Form::text('cedula', '', ['placeholder' => 'Cédula', 'class' => 'form-control', 'required']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::text('nombre', '', ['placeholder' => 'Nombre', 'class' => 'form-control', 'required']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::text('apellidos', '', ['placeholder' => 'Apellidos', 'class' => 'form-control', 'required']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::text('telefono', '', ['placeholder' => 'Teléfono', 'class' => 'form-control', 'required']) !!}
                      </div>

                      {!! Form::submit('Siguiente', ['class' => 'btn btn-info']) !!}

                      {!! Form::close() !!}

                  </div>
              </div>
          </div>

      </div>
  </div>
@endsection
