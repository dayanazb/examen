@extends('layouts.app')

@section('categories')
    <div class="container">
        {!! Form::open(['url' => '/category/'.$category->id, 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Editar categoría</h2>
              			<div class="card-header">
                			<div class="row">
                  				<div class="col-md-8">

                  					<div class="row">
							          <div class="col-md-4"></div>
							         	<div class="form-group col-md-4">
							            	<label for="description">Nombre:</label>
							            	<input type="text" class="form-control" name="description" value="{{$category->description}}">
							          	</div>
								        <div class="form-group">
					                        <label for="id_father">Categoría Padre:</label>
					                          <select class="form-control" name="id_father">
					                              @foreach($categories as $category)
					                              <option value="{{$category->id}}">{{$category->description}}</option>
					                              @endforeach
					                          </select>
					                      </div>
							        </div>

							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Actualizar</button>
							          </div>
							        </div>

                  				</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection