<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use DB;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $vehiculos = DB::table('vehiculos')->orderBy('id', 'desc')->paginate(10);
        return view('vehiculos')->with('vehiculos', $vehiculos);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_fotos()
    {    
        return view('fotos');
    }
 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Storage::putFile('image', new File('/path/public/image'));
        $c = new Vehiculo;
        $c->placa = $request->placa;
        $c->marca = $request->marca;
        $c->modelo = $request->modelo;
        $c->kilom = $request->kilom;
        $c->notas = $request->notas;
        $cliente =  DB::table('clientes')->find(DB::table('clientes')->max('id'));
        $c->id_cliente = $cliente->id;
        if ($c->save()) {
          return redirect()->action('VehiculoController@index');
        }
    }

    public function update(Request $request){
        dd($request);
        $vehiculo =  DB::table('vehiculos')->find(DB::table('vehiculos')->max('id'));
        $id = $vehiculo->id;
        DB::table('vehiculos')->where('id', $id)->update(['fotos' => $request->fotos]);
        return view('fotos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('products')->where('id', $id)->delete();
        return response()->json(['code' => 200]);
    }
}


