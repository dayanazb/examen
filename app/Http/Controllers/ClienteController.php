<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cliente;
use DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $clientes = DB::table('clientes')->orderBy('id', 'desc')->paginate(10);
        return view('clientes')->with('clientes', $clientes);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = new Cliente;
        $c->cedula = $request->cedula;
        $c->nombre = $request->nombre;
        $c->apellidos = $request->apellidos;
        $c->telefono = $request->telefono;
        if ($c->save()) {
          return redirect()->action('ClienteController@index');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *  In this case is used to update the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        $categories = DB::table('categories')->orderBy('id', 'desc')->paginate(10);
        return view('edit_category')->with(['category' => $category, 'categories' => $categories]);
    }

    public function update(Request $request, $id){
        DB::table('categories')->where('id', $id)->update(['description' => $request->description, 'id_father' => $request->id_father]);
        $categories = DB::table('categories')->orderBy('id', 'desc')->paginate(10);
        return view('categories')->with('categories', $categories);
    }



}
